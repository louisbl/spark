user:
    name: pigmonkey
    group: pigmonkey
    shell: /usr/bin/zsh
    email:  peter@havenaut.net
    log_dir: log

dotfiles:
    url: git@github.com:pigmonkey/dotfiles.git
    rcup_flags: -x README.md

aur:
    dir: aur
    packages:
        - aura-bin
        - pmount
        - orpie

ssh:
    port: 22
    user_key: /root/id_rsa
    enable_sshd: False

mail:
    sync_tool: isync
    sync_time: 10min
    sync_boot_delay: 2min
    sync_on: trusted

network:
    spoof_mac: True
    trusted_uuid:
        - 5eeb3104-5ad5-4072-a342-3691dfbbc27f
        - 11d2d0a0-d809-406b-b5c1-04bf3837cbf1
        - e5422190-e8a7-460f-b1ef-196c57036efa

editors:
    - gvim

video_drivers:
    - xf86-video-intel

base_packages:
    - tmux
    - git
    - gnupg
    - rsync
    - glances
    - task
    - coreutils
    - moreutils
    - pass
    - reflector
    - bc
    - ranger
    - strace
    - lsof
    - sysstat
    - arch-wiki-lite
    - python-keyring

filesystem_packages:
    - dosfstools
    - hfsprogs
    - ntfs-3g

browser_choices:
    - firefox -P default
    - firefox -P work
    - disposable chromium

passwordless_sudo:
    - /usr/bin/openvpn*

tarsnapper:
  deltas: 1h 6h 1d 7d 30d 365d
  timer:
    schedule: hourly
    run_on: trusted

gitannex:
    stopped_on: untrusted

bitlbee:
    run_on: trusted
